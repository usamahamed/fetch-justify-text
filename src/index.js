import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'

import Justify from './Justify'
import store from './store'

import './assets/index.css'


let demoComponent = <Justify />


ReactDOM.render((
  <Provider store={store}>
    {demoComponent}
  </Provider>
), document.getElementById('app'))
