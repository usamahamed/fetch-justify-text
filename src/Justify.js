import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { fetchAll } from './reducers/texts'
import BlockUi from 'react-block-ui';
//import 'react-block-ui/style.css';
import Header from './components/header'
import TextCom from './components/textsCom'

import $ from 'jquery'
 

class Justify extends React.Component {
  constructor(props) {
    super(props)
    localStorage.setItem('finish', JSON.stringify(0))

    this.boundHandleFetch = this.handleFetch.bind(this)
    this.handleRestore = this.handleRestore.bind(this);
    this.handledelete = this.handledelete.bind(this);
    this.handledelete = this.handledelete.bind(this)
    this.state = {  blocking: false,};
  }

  handleFetch() {
      // some simple jquary to organize dealing with App
      $('.delete_btn ').hide();$('.main_fetch').show(); 
      $('.temp_fetch').hide();$("input").attr("disabled", false);
      //Fetching Text from promise results
      this.props.actions.fetchAll()
      //Lock UI until fetching
      this.setState({blocking: !this.state.blocking});

  }

     componentWillReceiveProps(){
      //Unloack UI
      this.setState({blocking: !this.state.blocking});
        }

      handleRestore(){
              // some simple jquary to organize dealing with App
      $('.main_fetch').hide();$('.temp_fetch').show();$('.delete_btn').show();    
        // Check empty of stored texts
       if(JSON.parse(localStorage.getItem('Fetched_Store')) != null ){
       $("input").attr("disabled", true);
       var index=localStorage.getItem('index'); //index used for delete
        if (index === null) {index = 0; } 
         $('.hidden').val(index)  
         //justify based on previous fetch
    var htd= "<pre>"+new TextCom().justify(JSON.parse(localStorage.getItem('Fetched_Store'))[index]
      ,$('.pickval').val())+"</pre>"

    $('.temp_fetch').html(htd);
    let lenghcook=JSON.parse(localStorage.getItem('Fetched_Store')).length;
     if(index < lenghcook-1 && lenghcook!=0) { 
          index++;  
          localStorage.setItem('index', index);
    }
else{
        index=0;   
        localStorage.setItem('index', index);
}
}else{
    $('.temp_fetch').html('no Fetched text stored');
  }

  }



  handledelete(){
    //get stored fetched text
    let arr = JSON.parse(localStorage['Fetched_Store']);
    if(arr.length > 0){
      //map return json 
     var newarr=arr.filter(e => e !== arr[$('.hidden').val()]) 
     //get next fetched to delete     
   $('.temp_fetch').html(JSON.parse(localStorage.getItem('Fetched_Store'))[$('.hidden').val()]);   
   //reset values 
   localStorage.setItem('index', 0);
    $('.hidden').val(0)
    //stored new oject after delete
     localStorage.setItem('Fetched_Store', JSON.stringify(newarr));
   }else{
       $('.temp_fetch').html('no Fetched text stored');
   } 
  }
 

  render() {
    return (
      <div className="center">
        <Header />
        <main >

        <BlockUi tag="div" blocking={this.state.blocking}>
          <TextCom
            texts={this.props.texts}
            handleFetch={this.boundHandleFetch}
            handleRestoreProp={this.handleRestore}
            handledelete={this.handledelete}
            blockingstate={this.state.blocking}
          />
                  </BlockUi>

        </main>
              </div>
    )

  }
}

Justify.propTypes = {
  actions: PropTypes.object,
  texts: PropTypes.array,
}

const mapStateToProps = (state) => ({
  texts: state.texts,
})

const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators(
    { fetchAll },
    dispatch
  ),
})
export default connect(mapStateToProps, mapDispatchToProps)(Justify)
