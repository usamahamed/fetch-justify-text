import React from 'react'
import $ from 'jquery'

import PropTypes from 'prop-types'



class TextCom extends React.Component {

    constructor(props) {
          super(props);
         
    this.justify = this.justify.bind(this);       //bind justify function
    this.state = { arr_fetched_token :[]};

}
/*
Use spaces to fill in the gaps between words
Each line should contain as many as possible words
Use ‘\n’ to separate lines
‘\n’ is not included in the length of the line
Gaps between words can’t differ more than one space (e.g. 3, 3, 3, 2, 2 spaces)
Large gaps go first, then smaller: ‘Lorem---ipsum---dolor--sit--amet’ (3, 3, 2, 2 spaces)
Lines must not end by a space
Last line should not be justified, use only one space between words
Last line should not contain ‘\n’
*/

justify(str, len)
{
  var words = str.split(' ');
  var lines = [];
  var lastLine = words.reduce(function(line, word) {
    if (line) {
      if (line.length + word.length + 1 <= len)
        return line + ' ' + word;
      lines.push(line);
    }
    return word;
  });
  lines = lines.map(function(line) {
    if (line.indexOf(' ') >= 0)
      for (var lineLen = line.length; lineLen < len; )
        line = line.replace(/ +/g, function(spaces) {
          return spaces + (lineLen++ < len ? ' ' : '');
        });
    return line;
  });
  lastLine && lines.push(lastLine);
  return lines.join('\n');
}

/*
Every result from the API will be stored locally. The user sees a “restore” button appear for every loaded text. This allows the user to do the following:
Press any of these buttons to show the previously loaded/justified text in the main text window
Remove any of the history entries by pressing some remove icon
A maximum of 5 history entries will be recorded. After that, the oldest entry will be automatically removed.
When refreshing the browser, the history items will still be present
*/

  renderALLFetch() {
    var start = Math.random()*15;
    var end = Math.random()*200;
  
    return this.props.texts.map((photo) =>

     <pre> <div key={photo.id}> {this.justify(String(photo.about).substring(start,end),$('.pickval').val())}
      {this.state.arr_fetched_token.unshift(String(photo.about).substring(start,end))}
    {this.state.arr_fetched_token.splice(5,1)}
    { localStorage.setItem('Fetched_Store', JSON.stringify(this.state.arr_fetched_token))}
  
      </div>
      </pre>

    )
  }



  render() {
    return ( 
      <div>

        <button
          onClick={this.props.handleFetch}
          className="btn btn-primary Fetch_btn "
        >
          Justify Text
        </button>
         <label >
         <span className="restore_label"> Line length: </span>
          <input placeholder='Enter length before fetch' className="pickval" type="text" />
        </label>

        <button onClick={this.props.handleRestoreProp} className="btn btn-primary bg-gray restore_btn" >
          restore
        </button>
        <button  onClick={this.props.handledelete} className="btn btn-primary bg-red delete_btn" >
           Delete displayed cookie 
        </button>
        <div className="main_fetch" >

         {this.renderALLFetch()}
        </div>
        <div className="temp_fetch"> 
        </div>

      </div>
    )
  }
}

TextCom.propTypes = {
  texts: PropTypes.array,
  handleFetch: PropTypes.func,
  handleRestoreProp :  PropTypes.func,
  handledelete: PropTypes.func,
}

export default TextCom
