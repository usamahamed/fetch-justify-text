import React from 'react'
import LoadingBar from 'react-redux-loading-bar'

const Header = () => (
  <header>
    <LoadingBar />
    <a
      className="block mt3 mb4 mx-auto header_titel"
      style={{ fontSize: '2.25rem' }}
    >
      Simple-Text-Justify-App
    </a>
  </header>
)

export default Header
