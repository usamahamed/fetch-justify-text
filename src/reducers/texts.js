import fetch from 'isomorphic-fetch'
import $ from 'jquery'
import { shuffle, slice } from 'lodash'

const FETCH = 'texts_FETCH'
const Fetch_URL = 'http://www.json-generator.com/api/json/get/cePIyIJjJu?indent=2'

export const fetchAll = () =>  ({
  type: FETCH,
  payload: fetch(
    Fetch_URL,
   
    { cache: 'no-cache' }
  ).then((res) => res.json() 
  )

})


export default function fetchedReducer(state = [], action = {}) {

  switch (action.type) {
    case `${FETCH}_FULFILLED`:
    return slice( action.payload, 0, 1) 

    default: return state
  }
}










export function simulatefetchedReducer(state = [], action = {}) {

  switch (action.type) {
    case `${FETCH}_FULFILLED`:
    return slice( action.payload, 0, 1) 

    default: return state
  }
}