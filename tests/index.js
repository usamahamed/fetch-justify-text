
import Texts from '../src/components/textsCom'
import Header from '../src/components/header'
import Justify from '../src/justify'

import TestUtils from 'react-dom/test-utils'; 

import jest from 'jest';

import * as actions from '../src/reducers/texts'
import React from 'react'
import { SubmissionError } from 'redux-form'
import renderer from 'react-test-renderer';

// See README for discussion of chai, enzyme, and sinon
import chai, { expect } from 'chai'
import expect1 from 'expect'
import { shallow } from 'enzyme'
import chaiEnzyme from 'chai-enzyme'
import sinon from 'sinon'
import thunk from 'redux-thunk'
import configureMockStore from 'redux-mock-store'
import nock from 'nock'

chai.use(chaiEnzyme())
//test actions
const middlewares = [ thunk ]
const mockStore = configureMockStore(middlewares)
describe('async actions', () => {
  afterEach(() => {
    nock.cleanAll()
  })
    it('creates FETCH_TEXTS_SUCCESS when fetching text has been done', () => {
    nock('http://www.json-generator.com/')
      .get('api/json/get/cePIyIJjJu?indent=2')
      .reply(200, { payload: { texts: ['do something'] }})

    const expectedActions = [
      { type : ['texts_FETCH_PENDING','loading-bar/SHOW','loading-bar/HIDE','texts/FETCH_FULFILLED'] } 
    ]
    const store = mockStore({ type: [] })

    return actions.fetchAll()
     //.then(() => { // return of async actions
        expect(store.getActions()).toEqual(expectedActions)
    //  })
  })
  })

//test comonents
function setupHeader() {
 
      const enzymeWrapper = shallow(<Header />)
 return {
    enzymeWrapper
  }
}
function setup() {
  const props = {
  texts:[],
  handleFetch: fn => fn,
  handleRestoreProp : fn => fn,
  handledelete: fn => fn,
    }
      const enzymeWrapper = shallow(<Texts {...props}/>)
 return {
    props,
    enzymeWrapper
  }
}
//describe('components', () => {
  describe('Header', () => {
  	   it('Should Render as expect', () => {
      const enzymeWrapper = shallow(<Header />);

      expect(enzymeWrapper.find('a').hasClass('block')).to.be.equal(true);

      expect(enzymeWrapper.find('a').text()).to.be.equal('Simple-Text-Justify-App');


    })
})

describe('components', () => {
      const { enzymeWrapper } = setup()


      it('should render with expected texts', () => {
    expect(enzymeWrapper.find('.Fetch_btn').text()).to.be.equal('Justify Text')
        expect(enzymeWrapper.find('.restore_btn').text()).to.be.equal('restore')
    expect(enzymeWrapper.find('.delete_btn').text()).to.be.equal('Delete displayed cookie')
      const InputProps = enzymeWrapper.find('input').props()
      expect(InputProps.placeholder).to.be.equal('Enter length before fetch')

})
      it('simulate fetch button', () => {

            let callback = sinon.spy();
            const {props} = setup()
      const enzymeWrapper1 = shallow(<Texts {...props} handleFetch={callback}/>)
      	 let fetchbutton = enzymeWrapper1.find('.Fetch_btn')
          fetchbutton.last().simulate('click')
         expect(callback.calledOnce).to.be.true;

      })
 it('simulate restore button', () => {
   let callback = sinon.spy();
            const {props} = setup()
      const enzymeWrapper2 = shallow(<Texts {...props} handleRestoreProp={callback}/>)
      	 let restorebutton = enzymeWrapper2.find('.restore_btn')
      	 restorebutton.last().simulate('click');
         expect(callback.calledOnce).to.be.true;
      })
  it('simulate delete button', () => {
    let callback = sinon.spy();
            const {props} = setup()
      const enzymeWrapper3 = shallow(<Texts {...props} handledelete={callback}/>)
      	 let deletebutton = enzymeWrapper3.find('.delete_btn')
      	 deletebutton.last().simulate('click');
         expect(callback.calledOnce).to.be.true;
      })
      })


describe('Reducer::Texts', function(){
  it('returns an empty array as default state', function(){
    // setup
    let action = { type: 'unknown' };

    // execute
    let newState = actions.simulatefetchedReducer(undefined, { type: 'unknown' });

    // verify
    expect(newState).to.deep.equal([]);
  });
  
  describe('on LOADED_TEXTS', function(){
    it('returns given action', function(){
      // setup
      let action = {
        type:'texts_FETCH_FULFILLED' ,
        payload:{about:'Esse officia nostrud duis mollit fugiat dolore aliqua. Proident eiusmod consequat veniam pariatur exercitation officia cillum velit. Ad ullamco laboris enim consectetur excepteur non exercitation pariatur officia. Ut eu incididunt cillum deserunt ex labore laborum velit occaecat aliqua enim. Sint elit ut nisi incididunt dolor ipsum excepteur ullamco. Consectetur consequat et minim ut dolore. In amet laboris veniam voluptate magna non occaecat ut laboris enim aliqua.'
        ,
        	guid:"55b6978b-e2ba-4375-aa53-8fa7e731ce12" }};

      // execute
      let newState = actions.simulatefetchedReducer(undefined, action);
      // verify
      expect(newState).to.deep.to.be.defined;
    });
  });
});



//test provider and intgrated test for component
describe('Justify Test', () => {
    let renderer, state, store, props;
    beforeEach(() => {
        renderer = TestUtils.createRenderer();
        state = {
            texts: {
                about: 'Esse officia nostrud duis mollit fugiat dolore aliqua. Proident eiusmod consequat veniam pariatur exercitation officia cillum velit. Ad ullamco laboris enim consectetur excepteur non exercitation pariatur officia. Ut eu incididunt cillum deserunt ex labore laborum velit occaecat aliqua enim. Sint elit ut nisi incididunt dolor ipsum excepteur ullamco. Consectetur consequat et minim ut dolore. In amet laboris veniam voluptate magna non occaecat ut laboris enim aliqua.',
                guid:"55b6978b-e2ba-4375-aa53-8fa7e731ce12" 
            }
        };
        store = {
            subscribe: sinon.spy().named('subscribe'),
            dispatch: sinon.spy().named('dispatch'),
            getState: sinon.spy(() => state).named('getState')
        };
        props = {
            test: 'value',
            pageDefs: {} // required for TabPage
        };
    });

    it('renders a Texts with props and page name to show', () => {
        renderer.render( <Justify store={store} {...props}/> );
       return Promise.all([
            expect(renderer, 'to have rendered', <Texts {...props} />),
            expect(store.dispatch, 'was not called'),
            expect(store.subscribe, 'was not called'),
            expect(store.getState, 'was called')
        ]);
    });
});
