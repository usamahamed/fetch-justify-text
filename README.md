# Justify Text #

This is a small app for fetching text and justify it on the screen based on line length

### What is this repository for? ###

* React small app for justify text
* 1.0.0

### How do I get set up? ###

* git clone and cd to project
* npm install 
* run on localhost server


### Test ###
i also include some basic test for app with mocha you can run npm test

### Licence###
MIT