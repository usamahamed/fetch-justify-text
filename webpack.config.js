const path = require('path')
const ExtractTextPlugin = require('extract-text-webpack-plugin')

const sassLoaders = [
  'css-loader',
  'postcss-loader',
  'sass-loader?indentedSyntax=sass&includePaths[]=' + path.resolve(__dirname, './src')
]

module.exports = {
  devtool: 'eval-source-map',

  entry: [
    'babel-polyfill',
    './src',
  ],

  output: {
    path: path.join(__dirname, 'build'),
    filename: 'bundle.js',
  },

  plugins: [
    new ExtractTextPlugin({
      filename: 'bundle.css',
    }),
  ],

  resolve: {
    extensions: ['.js'],
    modules: [
      path.resolve(__dirname, 'src'),
      'node_modules'
    ],

    
  },

  module: {
   
    rules: [
      {
        test: /\.js$/,
        loader: 'babel-loader',
        include: path.join(__dirname, 'src'),
      },
      {
        test: /\.css$/,
        loader: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            {
              loader: 'css-loader',
              options: {
                importLoaders: 1,
                sourceMap: true,
              }
            },
            'postcss-loader',
          ],
        }),

      },
  {
            test: /\.scss$/,
      loader: 'style!css!sass?sourceMap',
              include: path.resolve(__dirname, './src'),


        }
    ],
  },
}
